package com.android.anothertask

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter


class ViewPagerAdapter(
    private val images: MutableList<Int>,
    private val imageListener: ImageListener
) : PagerAdapter() {

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val li = LayoutInflater.from(container.context)


        val view =
            li.inflate(R.layout.image_view, container, false)

        view.findViewById<ImageView>(R.id.iv_image).setImageResource(images[position])
        view.setOnClickListener {
            imageListener.imageOnClickListener(position)
        }
        container.addView(view)

        return view
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

}