package com.android.anothertask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.anothertask.databinding.FragmentFullImageBinding


class FullImageFragment : Fragment() {
    private lateinit var binding: FragmentFullImageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFullImageBinding.inflate(
            inflater, container, false
        )
        init()

        return binding.root
    }

    private fun init() {
        arguments?.getInt("image", -1)?.let { binding.ivFullImage.setImageResource(it) }
    }


}