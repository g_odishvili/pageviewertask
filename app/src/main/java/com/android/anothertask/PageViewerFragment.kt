package com.android.anothertask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.android.anothertask.databinding.FragmentPageViewerBinding

class PageViewerFragment : Fragment() {

    private var binding: FragmentPageViewerBinding? = null
    private var images = mutableListOf<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (binding == null) {
            binding = FragmentPageViewerBinding.inflate(
                inflater, container, false
            )

            init()
        }

        return binding!!.root
    }

    private fun init() {
        images.add(R.mipmap.kakashi)
        images.add(R.mipmap.itachi)
        images.add(R.mipmap.aoharu)
        binding?.viewPager?.adapter = ViewPagerAdapter(images, object : ImageListener {
            override fun imageOnClickListener(position: Int) {
                val bundle = bundleOf("image" to images[position])

                view?.findNavController()
                    ?.navigate(R.id.action_pageViewerFragment_to_fullImageFragment2, bundle)
            }

        })
    }

}